/*
  Copyright (c) 2013 Mats Sjöberg

  This file is part of the jsmandel programme.

  jsmandel is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  jsmandel is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with jsmandel.  If not, see <http://www.gnu.org/licenses/>.
*/

//-----------------------------------------------------------------------------
// Some parameters that can be changed below

var rows = 400;        // grid size
var cols = 600;

var box_w = 1;         // grid element size
var box_h = 1;

var max_it = 50;
var iter_steps = 10000;

//-----------------------------------------------------------------------------
// The rest are internal variables

var w;                // Model vectors, or weights
var ctx;              // Canvas context object

//-----------------------------------------------------------------------------

var log = function (msg) {
    setTimeout(function() {
        throw new Error(msg);
    }, 0);
};

//-----------------------------------------------------------------------------

var updateInfo = function(msg) {
    var infoP = document.getElementById('mandel_info');
    infoP.innerHTML = msg;
}

//-----------------------------------------------------------------------------

var colourString = function(x, y, z) {
    var r = Math.floor(255.0*x);
    var g = Math.floor(255.0*y);
    var b = Math.floor(255.0*z);
            
    return "rgb("+r+","+g+","+b+")";
};

//-----------------------------------------------------------------------------

var doMandel = function (r, c, n) {
    var done = false;
    while (!done && n > 0) {
        // x0 is the real axis: -2 .. +1
        var x0 = 3.0 * c / cols - 2.0;

        // y0 is the imaginary axis: -1 .. +1
        var y0 = 2.0 * (rows-r)/rows - 1;
        
        var x = x0;
        var y = y0;

        var i = 0;
            
        while ((x*x + y*y < 4) && (i < max_it)) {
            var xtmp = (x * x) - y * y + x0;
            y = 2.0 * x * y + y0;
            x = xtmp;
            
            i++;
        }
        
        var b = 1 - (i/50.0);
        ctx.fillStyle = colourString(b, 0, 0);
        ctx.fillRect(c*box_w, r*box_h, box_w, box_h);

        c++;
        if (c == cols) {
            c = 0;
            r++;
            if (r == rows)
                done = true;
        }
        n--;
    }
   
    if (done)
        updateInfo("Done!");
    else
        setTimeout(function () { doMandel(r, c, iter_steps); }, 0);
};

//-----------------------------------------------------------------------------

var init = function () { 
    updateInfo("Calculating mandelbrot...");

    var canvas = document.getElementById('mandel_canvas');
    if (!canvas.getContext)
        return;

    ctx = canvas.getContext('2d');

    setTimeout(function () { doMandel(0, 0, iter_steps); }, 0);
};

